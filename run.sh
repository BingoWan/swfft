#! /usr/bin/bash
mpirun --allow-run-as-root -np $1 --bind-to $3 --map-by $3 --report-bindings --mca pml ucx --mca btl ^vader,tcp,openib,uct --mca coll ^hcoll -x UCX_RNDV_THRESH=4k -x UCX_TLS=shm  ./build/TestDfft 1 $2 > $1.$2.timing
