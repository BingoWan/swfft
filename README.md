### SWFFT  
为了使得研究大尺度宇宙学程序在多样化平台上的大规模问题求解中获得获得高可扩展性和持续的性能。相关研究者开开发了宇宙学邻域中的跨平台加速优化框架（HACC）。HACC使用高阶谱算法来求解由大量示踪粒子产生的密度场的泊松方程。该方程的求解需要大量内存开销，为了解决这个问题，阿贡国家实验室Adrian Pope等人开发了一种“分布式内存、铅笔分解、并行的3维快速傅里叶变换”库。SWFFT将三维数据离散到块状的结构化网格中，做并行化处理的时候，再将数据重新分配到z,x,y,方向所对应的MPI进程中的“铅笔”网格中。在这种“块-铅笔”地转换后，每一个“铅笔”方向地网格再调用FFTW中的1维的FFT。此代码来源于https://xgitlab.cels.anl.gov/hacc/SWFFT，并将该代码移植到华为鲲鹏平台进行性能测试。
#### 编译及运行
- 1.	编译器及mpi
    鲲鹏：gcc的版本为9.2.0, mpi版本为4.0.2rc3
    X86:gcc版本5.3.0，mpi版本为4.0.2rc3
- 2.	安装FFTW库 
    - 下载相应的源码包(fftw-3.3.9.tar.gz)
	- cd ./fftw-3.3.8
	- ./configure --enable-shared
	- Make
	- make install
	- ldconfig
- 3.	安装SWFFT
   - 设置编译环境变量
        - 鲲鹏平台：
            export DFFT_MPI_CC=mpicc
            export DFFT_MPI_CXX=mpiCC
            export DFFT_MPI_FC=mpi90
        - X86,平台使用Inter编译器
            export DFFT_MPI_CC=mpiicc
            export DFFT_MPI_CXX=mpiicpc
            export DFFT_MPI_FC=mpiifort
    - 编译
        将编译选项改为-O3
        make
        make install
    - 设置运行环境变量
        export DFFT_FFTW_HOME=/path/to/fftw3
- 4.	应用运行
    编译完成后，会在build目录下生成可执行文件TestDfft。运行方法为mpirun -n 8 build/TestDfft \<n_repetitions\> \<ngx\> [ngy ngz]，其中\<n_repetitions\>测试case正向和反向的FFT的次数，\<ngx\>、\<ngy\>、\<ngz\>整个立方体三个方向的网格数。
